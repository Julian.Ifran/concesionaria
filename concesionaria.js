const formatMoney = (amount, decimalCount = 2, thousands = '.') => {
    try {
       let i = parseInt((amount = Math.abs(Number(amount) || 0))).toString();
       let decimalLimited = (amount = Math.abs(Number(amount) || 0).toFixed(decimalCount));
       decimalLimited = (decimalLimited + '').split('.')[1];
       let j = i.length > 3 ? i.length % 3 : 0;
       return (
          '$' +
          (j ? i.substr(0, j) + thousands : '') +
          i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands) +
          ',' +
          decimalLimited
       );
    } catch (e) {
       console.log(e);
       return 0;
    }
 };
 
 const cargarVehiculos = () => {
    return [
       new Auto("Peugeot","206",200000, 4),
       new Auto("Honda", "Titan", 60000,"125cc"),
       new Auto("Peugeot", "208", 250000,5),
       new Auto("Yamaha", "YBR",80500.50, "160cc"),
    ];
 };
 
 function Vehiculo(marca, modelo, Precio) {
    this.Marca = marca;
    this.Modelo = modelo;
    this.Precio = Precio;
    this.getFormatedPrice = () => {
       return formatMoney(this.Precio);
    };
 }
 function Moto(marca, modelo, Precio, Cilindrada) {
    Vehiculo.call(this, marca, modelo, Precio);
    this.Cilindrada = Cilindrada;
    this.toString = () => {
       return (
          'Marca: ' + marca + ' // Modelo: '+modelo+' // Cilindrada'+ Cilindrada + ' // Precio: ' + this.getFormatedPrice()
       );
    };
 }
 function Auto(marca, modelo, Precio, puertas) {
    Vehiculo.call(this, marca, modelo, Precio);
    this.puertas = puertas;
    this.toString = () => {
      return (
          'Marca: '+ marca+' // Modelo: '+modelo+' // Puertas: '+puertas+' Precio: '+
          this.getFormatedPrice()
       );
    };
 }
 
 function Concesionaria() {
    this.listaDeVehiculos = cargarVehiculos() || [];
    this.emitirReporte = (letraABuscar) => {
       listaDeVehiculos = this.listaDeVehiculos;
       this.imprimirVehiculos(listaDeVehiculos);
       console.log('=============================');
       let automascaro = this.masCaro(listaDeVehiculos);
       let automasbarato = this.masBarato(listaDeVehiculos);
       console.log(`Vehículo más caro:  ${automascaro.Marca} ${automascaro.Modelo} `);
       console.log(`Vehículo más barato: ${automasbarato.Marca} ${automasbarato.Modelo} `) ;
       this.encontrarLaLetra(letraABuscar,listaDeVehiculos);
       console.log('=============================');
       this.ordenarPorPrecio(listaDeVehiculos);
    };
    this.masCaro=(arrayVehiculos) =>{
      arrayVehiculos.sort((a, b) => (a.Precio < b.Precio ? 1 : -1));
      return arrayVehiculos[0];
   }
   this.masBarato=(arrayVehiculos) =>{
      arrayVehiculos.sort((a, b) => (a.Precio > b.Precio ? 1 : -1));
      return arrayVehiculos[0];
   }
   this.imprimirVehiculos=(arrayVehiculos) =>{
      arrayVehiculos.forEach((vehiculo) => {
         console.log(vehiculo.toString());
      });
   }
   this. encontrarLaLetra=(letra, arrayVehiculos) =>{
      let resultado;
      letra = letra.toUpperCase();
      arrayVehiculos.forEach(function (producto) {
         if (producto.Modelo.includes(letra)) {
            resultado = arrayVehiculos.filter((producto) => producto.Modelo.includes(letra));
            console.log(
               `Vehículo que contiene en el modelo la letra ${letra.toUpperCase()}: ${producto.Marca} ${producto.Modelo} ${formatMoney(producto.Precio)}`
            );
         }
      });
      if (resultado == undefined) {
         console.log(`Ningún Vehículo contiene la letra: ${letra.toUpperCase()}`);
      }
   }
   this. ordenarPorPrecio=(arrayVehiculos) =>{
      console.log('Vehículos ordenados por Precio de mayor a menor');
      arrayVehiculos.sort((a, b) => (a.Precio < b.Precio ? 1 : -1));
      arrayVehiculos.forEach((producto) => {
         console.log(`${producto.Marca}  ${producto.Modelo} `);
      });
   }
 }
 let concesionaria = new Concesionaria();
 concesionaria.emitirReporte("Y");



 
 
 
 
